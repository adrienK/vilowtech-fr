# 🛖 vilowtech.fr
Mise en place d'un site vitrine low-tech pour https://vilowtech.fr

## Utilisation
### Configuration globale
Le fichier `config.yaml` qui se trouve à la racine du projet permet de configurer les paramètres globaux.

La section `params` :
- `title` : le titre du site, par défaut `Exposition 100% Villette makerz`
- `params.icon` : l'icône du site, celui-ci doit être un [emoji](https://getemoji.com/)
- `params.lang` : la langue du site, utilisé pour la balise `html`
- `params.locale` : la locale du site, utilisé pour la meta `lang`
- `params.color` : la couleur du site, utilisé pour la meta `theme-color`
- `params.description` : la description du site, utilisé pour la meta `description`

Il est préférable de ne pas modifier les paramètres suivants :
- `theme` : le thème à utiliser, par défaut `livreblanc`
- `languageCode` : le code de la langue, par défaut `fr-fr`
- `baseURL` : l'URL du site, par défaut `https://vilowtech.fr`
- `environment` : l'environnement du constructeur, par défaut `production`
- `minify.minifyOutput` : la minification du site, par défaut `true`

### Création/Gestion des articles et pages
Tous les contenus rédactionnels sont stockés dans le dossier `content/`. Ajouter un nouveau fichier `.md` ajoute une nouvelle page au site. Les fichiers `_index.md` sont les pages par défaut de leur dossier respectif. Si un fichier est vide, sa page en ligne sera une page blanche (sauf pour les pages utilisant un ou des modules), s'il a un titre, on aura un titre de page, s'il a un contenu, on aura un contenu sur la page.

#### Création d'une nouvelle page
Pour créer une nouvelle page, il suffit de créer un nouveau fichier `.md` dans le dossier `content/`. Le nom du fichier doit être en minuscule et sans espace. Le nom du fichier sera utilisé pour l'URL de la page. Par exemple, pour créer une page `contact` il faut créer un fichier `content/contact.md`. Il est possible de créer des sous-dossiers pour organiser les pages, par exemple pour créer une page `contact` dans le dossier `recherches` il faut créer un fichier `content/recherches/contact.md`.

Il n'y a pas de limitations dans la création de page, pour les ajouter au menu voir la section suivante.

#### Gestion de l'Accueil
La page d'accueil est gérée par le fichier `content/_index.md`. Il est possible d'y ajouter du contenu, par exemple un texte d'accueil.

Pour gérer le contenu des articles de la page d'accueil, il faut modifier/ajouter/supprimer les fichiers `.md` dans le dossier `_modules/accueil/`. Le fonctionnement est le même que pour les pages, sauf qu'il ne faut pas oublier d'ajouter le paramètre `module.group` en mode `accueil` pour que le module soit affiché sur la page d'accueil.

#### Gestion de la page Recherches
La page Recherches est gérée par le fichier `content/recherches.md`. Même procédure que pour la page d'accueil, cependant les modules sont dans `_modules/recherche/` et l'attribut `module.group` doit être en mode `recherche`.

#### Gestion des expositions
Tout comme pour la page d'accueil et la page Recherches, il est possible d'ajouter un texte d'accueil. Pour cela, il faut modifier le fichier `content/exposant·e·s/_index.md`, cette page liste tous les exposant·e·s avec un lien vers leur page sous la forme d'une miniature et d'un titre.

Gestions des pages des exposant·e·s :
- Pour ajouter un exposant·e, créer un nouveau fichier `.md` dans le dossier `content/exposant·e·s/`.
- Pour modifier un exposant·e, modifier le fichier `.md` correspondant dans le dossier `content/exposant·e·s/`.
- Pour supprimer un exposant·e, supprimer le fichier `.md` correspondant dans le dossier `content/exposant·e·s/`.

Une page d'exposant doit absolument contenir 6 paramètres (Voir la section paramètres pour plus d'informations) :
- `title` : le titre de la page
- `description` : la description de la page
- `miniature` : l'image miniature pour la page de listing
- `exposition.nom` : le nom de l'exposition, utilisé dans la liste des explosions et en titre de la page
- `exposition.projet` : le nom du projet, sous-titre de la page
- `credit.nom` : le nom de l'auteur du projet

L'image de miniature doit être stockée dans le dossier `static/images/exposition/miniature/`, les images des pages des exposant·e·s doivent être stockées dans un sous-dossier du dossier dans `static/images/exposition/` au nom de l'exposition.

### Gestion des images
Toutes les images sont stockées dans le dossier `static/img/`. Il est possible de créer des sous-dossiers pour organiser les images, par exemple pour créer une image `logo.png` dans le dossier `recherches` il faut créer un fichier `static/img/recherches/logo.png`.

Pour appliquer un effet de dithering, il est possible d'utiliser cette outil : https://doodad.dev/dither-me-this/

Il est primordial d'optimiser le poids des images, pour cela il est fortement recommandé d'utiliser : https://compressor.io/

- Pour les miniatures, les images doivent être de taille 500px par 500px
- Les autres images n'ont pas besoin d'une taille supérieur à 1536px
- Utiliser le format JPG ou WebP en priorité, mais le format PNG pour les images ayant eu un traitement en dithering

### Configuration des pages
Chaque page ou article possède une partie configuration qui sont séparés par des `---`   
[R] Option requise | [O] Optionnel | [N] Ne pas modifier

Option génériques :
- [R] `title` : le titre de la page
- [R] `description` : la description de la page, utilisé pour la meta `description`
- [O] `menu` : pour ajouter la page au menu, doit être égal à `main`
- [O] `weight` : la position de la page dans le menu et l'article dans la liste des articles
- [O] `colonnes` : pour afficher la page en deux colonnes, doit être égal à `true`
- [O] `thumbnail` : l'image de la page, utilisé pour afficher une image de présentation
- [O] `notes` : une liste de notes, utilisé pour afficher des notes en bas de page (supporte le format [Markdown](https://www.markdownguide.org/))

Options spécifiques à l'accueil et recherches :
- [N] `module.load` : le module à charger, doit être égal à `accueil` ou `recherche`
- [R] `module.group` : le groupe auquel appartient l'article'
- [O] `action.label` : le label du bouton d'action a afficher
- [O] `action.url` : l'URL du bouton d'action

Options specifiques aux exposions :
- [R] `miniature` : l'image a afficher dans la liste des exposions
- [R] `exposition.nom` : le nom de l'exposition, utilisé dans la liste des exposions et en titre de la page
- [R] `exposition.projet` : le nom du projet, sous-titre de la page
- [R] `credit.nom` : le nom de l'auteur du projet
- [O] `credit.url` : l'URL de l'auteur du projet

Exemple :
```yaml
title: 100% low-tech
description: Evolution agricole via l’usage du micro-tracteur en kit.
menu: main
weight: 3
colonnes: true
thumbnail: images/main_header.png
notes:
    - Video de presentation https://www.youtube.com/watch?v=QNnrVzHnp_E
    - Crédits images Paul Appert

module: 
  load: accueil
  group: accueil
action:
  label: Découvrir les projets
  url: /low-tech

miniature: /images/exposition/miniature/cultiver.jpg
exposition:
  nom: Hors-champ
  projet: outil adapté aux nouvelles pratiques maraîchères
credit:
  nom: Paul Appert, Ensci Les Ateliers, 2019
  url: https://www.ensci.com/
```

### Configuration légale
Le fichier des mentions légales se trouve dans le dossier `content/about/mentions-legales.md`, il est possible de le modifier à sa guise dans le format [Markdown](https://www.markdownguide.org/). Les sections `Directeur de la publication`, `Édition du site` et `Hébergement` sont générées automatiquement.

Il est possible d'éditer deux de ces sections :
- `Directeur de la publication` via le fichier `data/publishers.yaml`
- `Hébergement` via le fichier `data/hosting.yaml`

### Configuration des "sponsor/partenaires"
L'images des sponsors sont stockées dans le dossier `static/images/sponsor.png` et sont référencées dans le fichier `data/sponsors.yaml`.

#### [static/images/sponsor.png] Fichier de sprite des sponsors
Chaques image de sponsors est au format 200x200px, ils sont regroupés en un sprite pour réduire le nombre de requête HTTP.
Pour y ajouter un sponsor, il suffit d'ajouter son image dans le sprite a la suite des autres.

Il est possible de retirer un sponsor sans modifier le sprite en lui ajoutant un attribut `hide: true` (_Voir partie suivante_).

#### [data/sponsors.yaml] Fichier de configuration des sponsors
La section `global` :
- `global.image` : l'image du sprite, par défaut `"/images/sponsor.png"`
- `global.size` : la taille d'affichage d'une image sur le site, par défaut `80px`

La section `list` permet de configurer les sponsors, il est possible d'en ajouter autant que nécessaire en respectant la syntaxe et le format de liste :
- `place` : la position du sponsor sur le site
- `position` : la position de l'image dans le sprite
- `alt` : le texte alternatif de l'image, utilisé si l'image ne peut pas être chargée
- `url` : l'URL optionnel du sponsor
- `hide` : pour cacher le sponsor, doit être égal à `true`

Exemple :
```yaml
global:
  image: /images/sponsor.png
  size: 80
list:
  - place: 2
    position: 1
    alt: Agence de la transition écologique
    url: https://www.ademe.fr/

  - place: 1
    position: 2
    alt: Agence Azimio
    url: https://www.azimio.fr/
```

_Attention : si un attribut contient le caractère `:`, il est nécessaire de l'entourer de guillemets._

## Structures du projet
```
📦vilowtech.fr/
 ┣ 📂content/               # Contenu rédactionnel du site
 ┃ ┣ 📂_modules/
 ┃ ┃ ┣ 📂accueil/           # Articles du module Accueil
 ┃ ┃ ┃ ┗ 📄...
 ┃ ┃ ┃
 ┃ ┃ ┗ 📂recherche/         # Articles du module Recherche
 ┃ ┃   ┗ 📄...
 ┃ ┃
 ┃ ┣ 📂about/               # Les pages concernant la vie du site
 ┃ ┃ ┗ 📄...
 ┃ ┃
 ┃ ┣ 📂exposants/           # La liste des expositions sous forme de blog
 ┃ ┃ ┣ 📄_index.md          # La page d'accueil des exposants
 ┃ ┃ ┗ 📄...
 ┃ ┃
 ┃ ┣ 📄_index.md            # La page d'accueil du site
 ┃ ┗ 📄...
 ┃
 ┣ 📂data/                  # Données organisées du site
 ┃ ┣ 📄hebergement.yml      # Informations legales sur l'hebergement
 ┃ ┣ 📄publication.yml      # Informations legale du directeur de la publication
 ┃ ┗ 📄sponsor.yml          # Configuration et informations de l'image de sponsor
 ┃
 ┣ 📂static/                # Fichiers accessibles depuis le site
 ┃ ┣ 📂documents/           # Des documents a partager
 ┃ ┃ ┗ 📄...
 ┃ ┃
 ┃ ┗ 📂images/              # Les images du site
 ┃   ┣ 📂expositions/       # Images des expositions
 ┃   ┃ ┣ 📂miniature        # Miniatures des images des expositions
 ┃   ┃ ┗ 📂...              # Un dosier par exposition
 ┃   ┃
 ┃   ┗ 📄...
 ┃
 ┣ 📂themes/                # dossier contenant le themes "livreblanc"
 ┃ ┗ ...
 ┃
 ┣ 📂tools/                 # les utilitaires de construction
 ┃ ┗ ...
 ┃
 ┣ 📄config.yaml            # Configuration du site
 ┣ 📄LICENSE                # license License-GPL 3.0 or later
 ┗ 📄README.md              # fonctionnement et definition du projet
 ```

## Technologies
La gestion du projet est assurée par [git](https://git-scm.com/)
- Largement utilisé pour la gestion de version de code
- Simple d'utilisation

Le constructeur de site statique de ce projet est [Hugo](https://gohugo.io/)
- Rapide et performant car dans un langage dit compilé
- Documentation complette
- Forte communautee
- Stable

Redaction du contenu en [Markdown](https://fr.wikipedia.org/wiki/Markdown)
- Pas de connaissances requises
- Syntaxe simple et lisible

Construction CSS du theme avec [SASS](https://sass-lang.com/) et plus particulierement [SCSS](https://sass-lang.com/documentation/syntax)
- Gestion de variables et dependances sans preprocesseur CSS3/4
- Forte optimisation du code CSS produit

### Scripts d'usage
- **To Do**