---
title: L’expo 100%
weight: 1
module: 
  group: accueil
action:
  label: Découvrir l'exposition
  url: /exposantes/
---

**100 %** est le nouveau rendez-vous de la jeune création instauré par La Villette. Pensée comme un tremplin pour les jeunes créateurs, **100%** donne à des pratiques artistiques variées une visibilité sur la scène parisienne et internationale tout en étant une occasion unique de dialoguer, d’échanger, de s’inspirer autour d’une synergie inédite. Pour sa cinquième participation à **100%** la Villette, Villette Makerz propose (du 31 mars au 18 avril) une exposition en ligne sur le thème du Low-Tech. 

**100% Low Tech** propose d’embarquer vers l’utopie contemporaine des nouveaux vocables du Design (où le soin et la planète apparaissent comme des horizons de redéfinition d’un humanisme), à bord d’un véhicule, celui du faire et de la culture qui prend forme et se transmet par la matière. 

Nous avons le plaisir d’y associer : L’école Boulle, l’Ecole National Supérieur de Création Industrielle, l’École nationale supérieure des Arts Décoratifs, et l’entreprise de l’ESS Terravox.