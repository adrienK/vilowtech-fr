---
title: Low tech ou technologie adaptée ?
weight: 2
module: 
  group: accueil
action:
  label: Decouvrir le livre blanc
  url: /documents/Livre_blanc.pdf
---

Depuis des décennies, de nombreuses réflexions sont menées sur les technologies dites « durables »  et accessibles à tous. Dans une optique de durabilité, les innovations « low-tech », par opposition  au « tout high-tech », s’inscrivent dans une démarche qui questionne nos besoins réels, interroge  nos modes d’organisation territoriale et renouvelle nos imaginaires : il s’agit de développer des  produits et services les moins intensifs et complexes en technologie et de limiter les impacts  environnementaux induits dans les usages, les comportements et les modes de consommation. 

Innovation « low-tech » ?

Une approche systémique de l’innovation, repensée au regard des usages réels et de sa capacité à transformer la société par de moindres  intensité et complexité technologiques et à produire de la résilience territoriale.

L’innovation « low-tech » prend comme point de départ l’usage et la durabilité en repensant  l’innovation d’abord sous contrainte de ressources puis, éventuellement, sous contrainte de coût, pour  développer des produits et des services plus simples, plus sobres en ressources et en énergie, plus  facilement recyclables, sans perte de matière et mieux proportionnés aux besoins satisfaits. Elle peut  aussi conduire à se passer du produit ou du service lui-même par une transformation sociétale ou  organisationnelle (la production locale pour éviter le transport, etc.). Elle vise donc à construire une  stratégie plus globale, plus systémique, davantage centrée sur les usages, dans un souci de qualité,  de soutenabilité et de résilience maximales.