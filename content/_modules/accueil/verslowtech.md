---
title: Vers une technologie Lowtech ?
weight: 3
module: 
  group: accueil
action:
  label: Découvrir les projets
  url: /exposantes/
---

La fabrique des imaginaires Low tech par l’Etablissement Public du Parc et de la Grand Halle de la Villette.

Villette Makerz et AZIMIO mènent un projet d’étude et d’expérimentation sur le parc de la Villette, lauréat de l’appel à Manifestation d’Intérêt (AMI) de la direction régionale Île-de-France de l’ADEME “Vers une innovation low tech en Ile de France – Pour une transformation systémique des territoires”.

L’objet de cet AMI est de promouvoir l’innovation low tech qui, par opposition aux high tech, s’incarne dans des projets sobres, peu consommateurs en ressources, et invitant à une réflexion systémique sur de nouvelles manières de vivre et de faire.

- Comment «Faire plus et mieux avec moins» adviendra-t-il le reflet d’une société rêvée ?
- Comment les imaginaires et les institutions culturelles peuvent être des leviers de la transition écologique ? 
- Comment mobiliser et mettre en scène la création  et la diffusion de nouveaux récits ?