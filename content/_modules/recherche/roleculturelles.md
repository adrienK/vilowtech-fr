---
title: Quel rôle ont les institutions culturelles dans la diffusion d’imaginaires low-tech ?
weight: 1
module: 
  group: recherche
---

Autrement dit, il s’agira de se poser la question de la responsabilité et du pouvoir des institutions culturelles dans le mouvement de réflexion sur les technologies et dans la construction de récits alternatifs venant façonner nos pensées et nos consciences, et éventuellement modifier nos façons de faire et d’agir. 

Nous nous intéressons pour cela aux différents outils des institutions culturelles, dont: 
- La programmation culturelle
- La scénographie et les outils d’exposition
- Les modalités d’accueil du public

Conçue comme un maillon de notre réflexion, l’exposition 100% est un outil de médiation en cours de construction qui permettra de mettre en débat ces questions. 