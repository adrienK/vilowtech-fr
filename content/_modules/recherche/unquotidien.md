---
title: Qu’est-ce qu’un quotidien low-tech ?
weight: 2
module: 
  group: recherche
---

C’est autour de cette question que s’articule le contenu de cette exposition. En abordant la question des low-tech par le prisme des modes de vie et des projections individuelles, la dimension technique ou techniciste des low-tech est mise de côté pour se concentrer sur nos expériences intimes avec les objets du quotidien.

En montrant des objets bien réels, conçus dans une démarche de sobriété technique et technologique, cette exposition questionne l’idée qu’une innovation est souvent un progrès technologique “high-tech” ou “numérique”. 

Ainsi, la force du travail des artistes exposé.e.s est d’ouvrir la voie à des possibilités d’existence alternatives et vraisemblables par le biais de nouveaux usages quotidiens. 