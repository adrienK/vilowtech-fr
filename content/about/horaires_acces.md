---
title: Horaires et accès
menu: main
weight: 40
---

## 📍 Informations pratiques
Toutes les informations pour venir nous voir.

### 📅 Les dates
Ho non, c'est déjà fini !   
Mais on vous donne rendez-vous bientot pour une prochaine édition.

## ✉️ Propositions de partenariats
Vous souhaitez proposer notre exposition dans vos locaux ?    
N'hésitez pas à [nous contacter !](/about/contactez_nous/)