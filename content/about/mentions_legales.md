---
title: Mentions légales
menu: main
weight: 90
---

<!-- Directeur de la publication -->
## 📝 Directeur de la publication
{{% credit "publishers" %}}

## 💻 Édition du site
{{% credit "theme_author" %}}

## 🏠 Prestataire d'hébergement
{{% credit "hosting" %}}

## 🍪 Traitement des données à caractère personnel
À fin de rester en accord avec ses engagements d'éthiques et de sobriété, le site vilowtech.fr n'a recours à aucune collecte de données ni à aucun cookie.

Les statistiques de fréquentation sont réalisées exclusivement depuis les données fournies par le service d'hébergement. Ces informations sont anonymisées et ne permettent pas de connaitre l'identité ou d'analyser le comportement d'un visiteur.

Dans le cas où un visiteur souhaiterait nous contacter, il est invité à se rendre sur la page de contact. Les données envoyées sont uniquement utilisées pour répondre à la demande du visiteur. Elles ne sont pas conservées et ne sont pas transmises à des tiers, sauf demande explicite de celui-ci.

## 📜 Propriété intellectuelle
Le contenu du site vilowtech.fr est entierement publié sous licence [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/).
- L'attributon du contenu des exposant·e·s est a la destination de l'exposant·e·s mentionné·e·s sur sa page.
- L'attibution du contenu des pages est a destination du [Directeur de la publication](#-directeur-de-la-publication).

Le design du site est publié sous licence [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/), et est basé sur le travail du [Directeur de la publication](#-directeur-de-la-publication) dans le cadre de la réalisation d'un [Livre Blanc sur l’étude de la culture low-tech](/documents/Livre_blanc.pdf)

Le code source du site est publié sous licence [GNU GPL v3](https://www.gnu.org/licenses/gpl-3.0.fr.html), pour y accéder faites une demande à l'[éditeur du site](#-édition-du-site).
