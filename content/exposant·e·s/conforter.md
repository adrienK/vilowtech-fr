---
title: "Conforter"
miniature: "/images/exposition/miniature/conforter.png"
description: "Conforter"
exposition:
  nom: "Conforter"
  projet: "Service pour l'isolation des corps"
credit:
  nom: "Camille Ferrer & Victoire Lesthevenon, Ensci Les Ateliers, 2021"
  url: ""
notes:
    - "Video de presentation https://youtu.be/2jccLlSbvyw"
---
Un service proposant des produits conçus pour l'isolation des corps et
de l'espace dans le cadre d'un habitat domestique, une parure pour le
télétravail assortie d'une garantie à l'achat et des tentures pour
palier les fuites thermiques, accessibles via une location sous
contrat. 

![Concept de conforter](/images/exposition/conforter/visuel1.jpg)

Nous sommes de plus en plus amenés à séjourner dans nos espaces
domestiques. Isoler est un point crucial pour diminuer notre
surconsommation de chauffage. Ce service de garantie et de location
assure la réparabilité et permet de s'intégrer dans une économie
circulaire. Les lainages en fin de vie retournés à l'entreprise
redeviennent une matière première et sont réinjectés dans la production.

Cette démarche permet de développer de nouvelles manières de produire et
de consommer tout en accédant à un confort lors de ses journées de
travail.

![Mise en situation](/images/exposition/conforter/visuel2.jpg)

Avec deux typologies de produits :-- une parure unisexe composée d'une
veste sans manche réchauffant le corps, accompagnée d'éléments en
mailles amovibles qui isolent les extrémités tout en offrant une aisance
dans les mouvements.

Des tentures au format de 40 cm de large, ajustables en longueur. Elles
sont placées devant les fenêtres et sont modulables grâce à un système
de glissière.

Le tout est en 100% laine dans les couleurs naturelles des toisons

![Pieces de tissue](/images/exposition/conforter/visuel3.jpg)

Ce projet, conçu et structuré autour d'une réflexion commune, propose
des réponses et des regards multiples, ouverts sur des possibles plus
responsables. Il s'appuie sur une cartographie de besoins et de
fonctionnalités textiles en intégrant un cahier des charges low-tech
défini par quelques grands principes : frugalité et économie à la
source, conception basée sur des techniques durables et réparables,
conditions de production s'appuyant sur le savoir et un travail digne.
Il privilégie la sobriété d'usage + la qualité d'un bien ou d'un
service.

Il se lit comme un ensemble de recherches, un cahier d'idées, de
matières, de scénarios, d'inspirations : des rêves avec leur mode
d'emploi, des utopies concrètes. Il a été mené dans le cadre d'un
Atelier de Projet d'un se- mestre à l'ENSCI-Les Ateliers, dans le
département textile, d'octobre 2020 à janvier 2021, par : *Alice
Bertrand, Nina Capron, Ines de Chefdebien Zagarriga, Floriane Delaville,
Camille Ferrer Gueldry, Camille Foucher, Manon Jacob, Lucie Le Jeune,
Cécile Lenoble, Anaïs Le Gou- pil, Victoire Lesthevenon, Laurie Sapina*

Encadrées par *Hélène Lemaire, Marion Lévy,* Et la collaboration de *Vanessa Goetz, Tony
Jouanneau, Hélène Lemaire, Marion Lévy, Giovanna
Massoni*.

