---
title: "Cuisiner"
miniature: "/images/exposition/miniature/cuisiner.png"
description: "Le muscle, lengrenage et la carotte"
exposition:
  nom: "Le muscle, lengrenage et la carotte"
  projet: "Moulin multifontion manuel"
credit:
  nom: "Antoine Pateau, Ensci, 2019"
  url: ""
notes:
    - "Video de presentation https://youtu.be/Vs7Cl8xUaTs"
---
La cuisine domestique est le théâtre du sur-équipement. Au regard de
produits souvent futiles, ce projet optimise les solutions anciennes
avec un savoir-faire moderne. L'objet proposé, un « moulin »
multifonction manuel, est accroché et assumé au mur, libérant ainsi le
plan de travail.

On actionne un levier dont le mouvement est décuplé par un mécanisme
interne, qu'un volant d'inertie visible fait perdurer. Ce mouvement est
renvoyé au dessus d'un saladier ou d'un verre doseur standards. On vient
brancher sur l'embout sortant du moulin l'ustensile souhaité.

![Design de reference](/images/exposition/cuisiner/visuel1.jpg)

Les formes du moulin sont nées d'une mise en valeur murale et graphique
de certains organes mécaniques que l'on rend à nouveau visible. L'aspect
des ustensiles s'est quand à lui définit par un souci de rangement,
d'entretien et de grande efficacité à faible vitesse
rotative.

Le muscle, pour rendre l'usager énergétiquement indépendant et redonner
de la valeur à une gestuelle autre qu'un bouton de commande.
L'engrenage, pour permettre à ce geste de ne pas retomber dans une
corvée du passé, grâce à un système mécanique qui démultiplie l'effort.
La carotte, un aliment simple qui parle de fonctions fondamentales.

|||
|-|-|
|![Utilisation en fouet](/images/exposition/cuisiner/visuel2.jpg)|![Utilisation en rape](/images/exposition/cuisiner/visuel3.jpg)|

