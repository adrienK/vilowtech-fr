---
title: Cultiver
miniature: /images/exposition/miniature/cultiver.png
description: Evolution agricole via l’usage du micro-tracteur en kit.
exposition:
  nom: Hors-champ
  projet: outil adapté aux nouvelles pratiques maraîchères
credit:
  nom: Paul Appert, Ensci Les Ateliers, 2019
  url: https://www.ensci.com/
notes:
    - Video de presentation https://www.youtube.com/watch?v=QNnrVzHnp_E
    - Crédits images Paul Appert
---

La population agricole se renouvelle, s’installe sur des fermes de petites tailles, cultive une production à forte valeur ajoutée. L’agriculture qu’elle pratique se souhaite plus respectueuse de l’écosystème et de la vie du sol. C’est une pratique exigeante qui requiert un travail de précision. Si les usages et les fermes se transforment, l’outillage utilisé doit également évoluer.

![mise en situation](/images/exposition/cultiver/situation.jpg)

Le micro-tracteur est un véhicule léger, électrique ne dégageant ni gaz ni bruits, permettant un travail adapté aux pratiques culturales du maraîchage biologique et diversifié. En complément d’un tracteur traditionnel, il effectue un travail en surface : désherbage, assistance à la récolte, faux semis, paillage, transport de charges. Il est conçu avec une économie de moyens et propose une construction en kit à mi-chemin entre la production en série et l’auto-construction.

Conception et réalisation d’un prototype, animation d’ateliers de conception / En collaboration avec Gaspard Tiné Bérès et le GRAB Bio centre / 2019

![visuel du projet](/images/exposition/cultiver/visuel.jpg)
