---
title: "Échanger"
miniature: "/images/exposition/miniature/echanger.png"
description: "L’Atelier du troc, est un atelier d’échanges de pièces en céramique itinérant."
exposition:
  nom: "Latelier du troc"
  projet: "Une production de convivialité"
credit:
  nom: "Isis Truphème, ESAA Boulle, 2020 "
  url: ""
notes:
    - "Video de presentation https://youtu.be/hFlSGNAEyK4"
---
"**L'Atelier du troc**" est un atelier d'échanges de pièces en céramique
itinérant.

À l'heure où nos modes de consommation sont remis en question, ce
projet propose de produire dans la convivialité afin de prendre
conscience du temps et de la valeur d'une production. Une convivialité
qui fait passer l'être avant l'avoir, pour faire
ensemble.

## TrocMobile

|||
|-|-|
|![TrocMobile](/images/exposition/echanger/trocmobile1.jpg)|![TrocMobile en place](/images/exposition/echanger/trocmobile2.jpg)|

Je suis partie du travail d'Ivan Illich qui estime que pour atteindre
une société conviviale il nous faut des outils de production qui soit
eux même conviviaux.

Des outils conviviaux sont des outils qui sont compréhensibles et
adaptables, des outils maîtrisables par le plus grand
nombre.

## TrocVélo

|||
|-|-|
|![TrocVélo](/images/exposition/echanger/trocvelo1.jpg)|![TrocVélo en place](/images/exposition/echanger/trocvelo2.jpg)|

« L'Atelier du troc » invite dans différentes situations à bénéficier
d'une pièce achevée si vous réalisez vous-même en échange une nouvelle
céramique en terre crue. C'est un projet qui fonctionne de façons
simple, accessible et grâce à l'interdépendance entre les participants.

L'atelier se tient alors dans plusieurs endroits, pour disséminer ces
instants de convivialité un peu partout. Il va à la rencontre de
personnes singulières pour nourrir la production de sensibilités
plurielles. Il est aussi un moyen de proposer une nouvelle forme de
partage des savoirs, une forme horizontale dans laquelle différentes
sources de connaissance s'enrichissent mutuellement.

|||
|-|-|
|![Troc a pied](/images/exposition/echanger/visuel1.jpg)|![Troc a pied en place](/images/exposition/echanger/visuel2.jpg)|