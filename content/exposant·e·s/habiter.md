---
title: "Habiter"
miniature: "/images/exposition/miniature/habiter.png"
description: "Projet de design d'élaborer imaginaire depuis des matières et des ressources"
exposition:
  nom: "Réseaux Disponibles"
  projet: "Le Pavillon des Rêves"
credit:
  nom: "Pablo Bras, Ensci Les ateliers, 2018"
  url: "https://www.ensci.com/"
notes:
    - Video de presentation https://www.youtube.com/watch?v=NDRo2WdTvWQ"
    - Crédits photos Véronique Huyghe"
---

Réseaux disponibles est un projet de design initié en 2017 et qui part des phénomènes que nous rencontrons chaque jour sans en profiter : torrent d’eau le long d’une gouttière, effet de serre dans un vasistas en plexiglass, rebond d’ondes dans une parabole etc… En partant de ces possibilités qui résultent d’activités naturelles et artificielles, je tente de les relier à des besoins ou des envies que nous éprouvons chaque jour.

![Visuel exposition avec projection](/images/exposition/habiter/visuel1.jpg)

Pour produire les objets-vecteurs qui raccordent ces phénomènes à nos quotidiens, j’emploie un maximum les ressources environnantes avant de peu à peu ouvrir le rayon des sollicitations et des imports. 

![Visuel exposition objets](/images/exposition/habiter/visuel2.jpg)

On obtient ainsi un répertoire de formes et de principes techniques où se mêlent procédés primitifs et actuels. Cette phase de recherche se nourrit de références issues de la culture maker et est inspirée de chaînes Youtube telles que Barnabé Chaillot, Green Power Science ou Primitive Technology.

![Visuel exposition avec projection 2](/images/exposition/habiter/visuel3.jpg)

Suite à ces expérimentations, j’élabore des scenarios où ce répertoire de principes techniques se confronte à des contextes et se modifient à leurs contacts. La proposition prend la forme d’un Diorama. Le but de cette démarche est de partir des matières et des ressources pour élaborer un imaginaire

![Visuel exposition avec projection 3](/images/exposition/habiter/visuel4.jpg)