---
title: "Indiquer"
miniature: "/images/exposition/miniature/indiquer.png"
description: "Enseignes auto-suffisantes fonctionnant grâce aux sources d’énergies discrètes"
exposition:
  nom: "Vire-volt"
  projet: "série d'enseignes auto-suffisantes fonctionnant grâce aux sources d'énergies discrètes"
credit:
  nom: "Canel Averna, Ensci, 2019/2020"
  url: ""
notes:
    - ""
---

Depuis l'apparition du tube néon, le monde de l'enseigne lumineuse s'est
rué vers des solutions toutes électriques, symboles de la modernité.
Dans ce brouhaha lumineux, la parole semble revenir à ceux qui éclairent
le plus fort. Pourtant la consommation énergétique engendrée par cette
escalade, et la mise en danger de la biodiversité des villes alertent
les scientifiques: il est l'heure d'éteindre un peu les lumières.
Certains projets de loi ont tenté d'imposer des restrictions. Pourtant,
sans alternatives attractives, les commerçants rechignent à disparaître
dans la nuit.

![Visuel d'une croix de pharmacie](/images/exposition/indiquer/visuel1.jpg)
_Maquette de croix de pharmacie montée sur un axe/ PMMA thermo-plié, fixations en ABS, laiton, roulements. 2019-2020_

Ce constat nous amène aux enjeux qui animent notre travail : comment le
design peut-il accompagner et encourager une législation favorisant la
transition écologique ? Comment peut-il venir en support de
problématiques sociétales et citoyennes ? Et comment faire signe
autrement, avec humour et astuce, dans la jungle lumineuse qui sollicite notre attention nuit
et jour?

|||
|-|-|
|![Visuel tabac](/images/exposition/indiquer/visuel2.jpg)|![Visuel pharmacie](/images/exposition/indiquer/visuel3.jpg)|

Vire-volt est une série d'enseignes auto-suffisantes fonctionnant grâce
aux sources d'énergies discrètes qui nous entourent déjà, et qui ont
toujours été là: la lumière naturelle, le vent, les mouvements
mécaniques du quotidien et l'électricité que la ville mobilise déjà, et
dont on peut optimiser l'utilisation.

![Recherche design](/images/exposition/indiquer/visuel4.jpg)

