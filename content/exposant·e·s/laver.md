---
title: "Laver"
miniature: "/images/exposition/miniature/laver.png"
description: "Jours de lessive"
exposition:
  nom: "Jours de lessive"
  projet: "Collection d'objets alternatifs visant à encourager des pratiques plus conscientes et responsables autour de la corvée du linge."
credit:
  nom: "Morgane Liger, ESAA Boulle, 2020"
  url: ""
notes:
    - "Video de presentation https://www.youtube.com/embed/z2AjjOJjJqY"
---

La démocratisation des machines à laver en Occident dans les années 50 a
permis de nous débarrasser d'une corvée pénible et chronophage, à un
point tel que cette dernière ne se résume plus qu'à la seule utilisation
de cet appareil.

![Image de lavelinge](/images/exposition/laver/visuel1.jpg)

Or de nombreux enjeux d'ordre financier, écologique, culturel et social
découlent de cette corvée, et leurs conséquences, dépassant très souvent
la frontière du domestique, méritent qu'on s'y intéresse.

En investissant 4 thématiques (fabrication de sa lessive, entretien de
son lave-linge, lavage à la main, partage des machines à laver), la
collection propose une redéfinition du sens de l'effort volontaire, à
l'échelle individuelle comme collective. La complémentarité entre objets
manufacturés et objets à récupérer promeut ainsi un design au service de
l'effort.

|   |   |
| - | - |
| ![Kit de lavage](/images/exposition/laver/visuel2.jpg) | ![Kit en mode usage](/images/exposition/laver/visuel3.jpg) |

![Visuel en graph](/images/exposition/laver/visuel4.jpg)
