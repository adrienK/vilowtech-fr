---
title: "Panser"
miniature: "/images/exposition/miniature/panser.png"
description: "Gamme de compresses et une gamme de filets de fixations"
exposition:
  nom: "Panser"
  projet: "Gamme de compresses et une gamme de filets de fixations."
credit:
  nom: "Alice Bertrand et Laurie Sapina, Ensci, 2021"
  url: ""
notes:
    - "Video de presentation https://youtu.be/7CuRoYk61Hg"
---
Lorsqu'on se blesse on se sent vulnérable, on a mal, panser les
blessures du quotidien nous permet de prendre soin de soi pour soi, et
de se traiter avec respect.

|||
|-|-|
|![Compresses](/images/exposition/panser/visuel1.jpg)|![Filets de fixations](/images/exposition/panser/visuel2.jpg)|

Aujourd'hui ces produits de soin sont des produits à usage unique,
jetables, en matières synthétiques. Ils sont source de déchets et
peuvent provoquer des allergies ou d'autres réactions.

Ces deux gammes sont en coton, doux, agréable sur la peau, et
hypoallergénique, recyclables et biodégradables. Elles sont lavables en
machine à 60°C ce qui permet de les stériliser et de les réutiliser. Une
ligne colorée signale la longévité en disparaissant après un certain
nombre de lavages.

![Bandelettes](/images/exposition/panser/visuel3.jpg)

Ce projet, conçu et structuré autour d'une réflexion commune, propose
des réponses et des regards multiples, ouverts sur des possibles plus
responsables. Il s'appuie sur une cartographie de besoins et de
fonctionnalités textiles en intégrant un cahier des charges low-tech
défini par quelques grands principes : frugalité et économie à la
source, conception basée sur des techniques durables et réparables,
conditions de production s'appuyant sur le savoir et un travail digne.
Il privilégie la sobriété d'usage + la qualité d'un bien ou d'un
service.

Il se lit comme un ensemble de recherches, un cahier d'idées, de
matières, de scénarios, d'inspirations : des rêves avec leur mode
d'emploi, des utopies concrètes. Il a été mené dans le cadre d'un
Atelier de Projet d'un se- mestre à l'ENSCI-Les Ateliers, dans le
département textile, d'octobre 2020 à janvier 2021, par : *Alice
Bertrand, Nina Capron, Ines de Chefdebien Zagarriga, Floriane Delaville,
Camille Ferrer Gueldry, Camille Foucher, Manon Jacob, Lucie Le Jeune,
Cécile Lenoble, Anaïs Le Gou- pil, Victoire Lesthevenon, Laurie
Sapina* ;

Encadrées par *Hélène Lemaire, Marion Lévy, *Et la collaboration
de *Vanessa Goetz, Tony Jouanneau, Hélène Lemaire, Marion Lévy, Giovanna
Massoni*.

