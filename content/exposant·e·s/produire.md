---
title: "Produire"
miniature: "/images/exposition/miniature/produire.png"
description: "Le recyclage et le réemploi des plastiques et des biomatériaux"
exposition:
  nom: "L'unité mobile"
  projet: "Une unité pédagogique mobile, utilisant le recyclage et le réemploi des plastiques et des biomatériaux"
credit:
  nom: "Xavier Garcia et Redouane Souni"
  url: "http://monstre.xyz/"
notes:
    - "Video de presentation https://youtu.be/5hjPHknHlXo"
---

Looplab est une unité pédagogique mobile, utilisant le recyclage et le
réemploi des plastiques et des biomatériaux comme un outil de médiation
pour sensibiliser à l'amélioration des gestes de tri, embellir les
espaces publics comme domestiques, et informer sur les métiers de
l'économie circulaire.

|||
|-|-|
|![Tri des plastiques](/images/exposition/produire/visuel1.jpg)|![Usage artistique](/images/exposition/produire/visuel2.jpg)|

Ce projet développé en partenariat par Terravox et Villette Makerz prend
appui sur la communauté Open source Precious Plastic pour créer un
dispositif pédagogique et de médiation citoyenne qui a pour vocation de
se déployer en Ile de France avant d'envisager un essaimage en France et
à l'étranger basé sur l'open source.

![Visuel de la remorque](/images/exposition/produire/visuel3.jpg)

Terravox intervient en ile de France au nom de la collectivité pour
sensibiliser les citoyens à la prévention des déchets et accompagne les
collectivités pour les aider à mieux comprendre les pratiques et les
attentes des citoyens.

![Velo de broyage](/images/exposition/produire/visuel4.jpg)

15 parcours distincts représentant plus d'une centaine d'heures sont
proposés. De l'intervention de rue en 10 min à l'atelier de plusieurs
jours pour fabriquer in situ du mobilier urbain à partir des collectes
locales, ce dispositif de médiation s'intègre à une démarche citoyenne
qui commence par une communication et des interventions préparatoires du
'projet local' et qui se termine avec des bénéfices tangibles restant au
profit de la communauté locale. Le looplab est le support d'une projet
d'amélioration du cadre de vie basé sur la participation.

![Schematisation du parcours](/images/exposition/produire/visuel5.jpg)

