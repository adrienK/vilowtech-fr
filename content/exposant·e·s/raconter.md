---
title: "Raconter"
miniature: "/images/exposition/miniature/raconter.png"
description: "Papier machine"
exposition:
  nom: "Papier machine"
  projet: "Collection de cahiers qui permettent d'explorer les fonctionnements invisibles de l'électronique."
credit:
  nom: "Raphaël Pluvinage et Marion Pinaffo, Ensci Les Ateliers, 2018/2019"
  url: ""
notes:
    - "Video de presentation https://youtu.be/TIX5T53Upwg"
    - "Papier Machine n°0, 2018-2019 Pinaffo---Pluvinage Precutbook, Offset 3 colors, conductive ink, electronics 25 x 35 cm, 48 pages"
---

 Les objets électroniques de notre quotidien abritent un monde qui peut
paraître mystérieux.

![Representation de la collection](/images/exposition/raconter/visuel1.jpg)

Sur nos téléphones, le basculement automatique du format portrait au
paysage semble magique, inexplicable. En réalité, il s'agit simplement
d'un gyroscope niché dans l'appareil. C'est également un capteur interne
qui permet au téléphone d'indiquer s'il a pris l'eau, s'il est secoué,
s'il est collé à notre oreille, s'il fait nuit, etc.  Ces composants ont
des qualités a priori invisibles, intangibles et incompréhensibles. Mais
observés de près, ils peuvent révéler leurs fonctionnements.

![Exemple livre](/images/exposition/raconter/visuel2.jpg)

Papier Machine n°0 est le premier numéro d'une collection de cahiers qui
permettent d'explorer les fonctionnements invisibles de l'électronique.
Ce premier cahier rassemble six jeux électroniques en papier prédécoupé,
sérigraphié avec une encre qui conduit l'électricité. 

![Exemple carte](/images/exposition/raconter/visuel3.jpg)

