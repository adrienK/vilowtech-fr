---
title: "Rafraîchir"
miniature: "/images/exposition/miniature/rafraichir.png"
description: ""
exposition:
  nom: "Les textiles"
  projet: "Quand coton et laine font la fraîcheur"
credit:
  nom: "Ines de chefdebien, Floriane Delaville, Ensci Les Ateliers, 2021"
  url: ""
notes:
    - "Video de presentation https://youtu.be/LLAk3LXgA8w"
---

Une gamme de textiles offrant une sensation de fraîcheur. Leurs formes
et leurs usages varient. Dans un contexte de plage, certains recouvrent
le corps tout en le protégeant des UV. D'autres, dans un contexte plus
urbain, s'utilisent de manière ponctuelle afin de nettoyer le visage et
de rafraîchir.

|||
|-|-|
|![Concept de rafraîchir](/images/exposition/rafraichir/visuel1.jpg)|![Mise en situation](/images/exposition/rafraichir/visuel2.jpg)|

Face au réchauffement climatique et aux épisodes caniculaires de plus en
plus nombreux, il est indispensable d'installer des routines et des
gestes usuels pour atténuer les effets de grande chaleur de manière plus
responsable.

Par un simple jeu de matières naturelles et biodégradables, par des
densités étudiées, ces textiles s'imbibent d'eau et diffusent de la
fraîcheur. Le coton travaillé en monomatière permet la recyclabilité des
produits. Associé à la laine, il offre un rafraîchissement ciblé. 

![Tissu et eau](/images/exposition/rafraichir/visuel3.jpg)

Ce projet, conçu et structuré autour d'une réflexion commune, propose
des réponses et des regards multiples, ouverts sur des possibles plus
responsables. Il s'appuie sur une cartographie de besoins et de
fonctionnalités textiles en intégrant un cahier des charges low-tech
défini par quelques grands principes : frugalité et économie à la
source, conception basée sur des techniques durables et réparables,
conditions de production s'appuyant sur le savoir et un travail digne.
Il privilégie la sobriété d'usage + la qualité d'un bien ou d'un
service.

Il se lit comme un ensemble de recherches, un cahier d'idées, de
matières, de scénarios, d'inspirations : des rêves avec leur mode
d'emploi, des utopies concrètes. Il a été mené dans le cadre d'un
Atelier de Projet d'un se- mestre à l'ENSCI-Les Ateliers, dans le
département textile, d'octobre 2020 à janvier 2021, par : *Alice
Bertrand, Nina Capron, Ines de Chefdebien Zagarriga, Floriane Delaville,
Camille Ferrer Gueldry, Camille Foucher, Manon Jacob, Lucie Le Jeune,
Cécile Lenoble, Anaïs Le Gou- pil, Victoire Lesthevenon, Laurie
Sapina* ;

Encadrées par *Hélène Lemaire, Marion Lévy,* Et la collaboration
de *Vanessa Goetz, Tony Jouanneau, Hélène Lemaire, Marion Lévy, Giovanna
Massoni*.

