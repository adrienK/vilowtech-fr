---
title: "Re Cuisiner"
miniature: "/images/exposition/miniature/cuisiner_2.png"
description: "Re"
exposition:
  nom: "Re"
  projet: "du mini-électroménager repensé pour durer"
credit:
  nom: "Romain Poivey, Ensci Les Ateliers, 2020"
  url: ""
notes:
    - "Video de presentation https://youtu.be/e3BzP7cW3Eo"
---

>Re:parable  
Re:habilitable   
Re:ajustable  
Re:modelable   
Re:utilisable  

**Re:pensé pour durer**

**« Re »** mène une réflecion sur le mini-électroménager de moyenne
gamme.

Cette démarche s'inscrit dans un contexte nouveau où le comportement des
usagers commence à peser et à remettre en question l'actuel modèle de
consommation; acheter à petit prix, utiliser, casser, jeter et racheter
à petit prix.

La réparation de ces petits appareils, actuiellement difficile pour des
raisons de rentabilité, devient un des enjeux majeur. Elle se voit
soutenue par des décisions politiques fortes qui tendent à rendre
obsolète le système en place.

![Presentation des cones](/images/exposition/cuisiner_2/visuel1.jpg)

Pour les industriels, cela pose de nouvelles problématiques, notamment
en termes de sourcing des pièces détachées. 

L'objectif est donc de repenser le dessin des appareils mais aussi, plus
globalement, de penser le système dans lequel ils s'insèrent afin de
trouver une alternative bénéfique à chaque acteur; usagers, réparateurs
et industriels. 

![Visuel des formes](/images/exposition/cuisiner_2/visuel2.jpg)

Pour ce faire, le projet s'appuie prinsipalement sur la transversalité
des pièces. Elle a pour objectif de permettre un jeu de compositions, de
mutualisations de l'électronique et des capotages entre appareils,
minimisant ainsi le nombre de pièces dites spécialisées.

Les objets en résultant, d'apparence élémentaire, sont contraits par des
règles de conception strictes et enchevêtrées. Ces règles ont permis
progressivement de qualifier différemment les usages et les formes,
tentant de leur faire révéler un tout nouveau potentiel, où leurs durées
de vies et leurs destinés s'en trouvent augmentées. 

|||
|-|-|
|![Assemblage des formes](/images/exposition/cuisiner_2/visuel3.jpg)|![Assemblage des formes concept](/images/exposition/cuisiner_2/visuel4.jpg)|

