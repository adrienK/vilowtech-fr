---
title: "S'exercer"
miniature: "/images/exposition/miniature/s_exercer.png"
description: "Gamme d’accessoires de sport pour le télétravail"
exposition:
  nom: "Post-fit"
  projet: "Gamme d'accessoires de sport pour le télétravail."
credit:
  nom: "Anaïs Le Goupil & Camille Foucher, Ensci Les Ateliers, 2021"
  url: ""
notes:
    - "Video de presentation https://youtu.be/NKVzTSicJxE"
---
 Un système de sangles fixé sur une chaise assure une bonne posture et
permet de pratiquer des exercices physiques à son poste de travail. 

![Mise en situation](/images/exposition/s_exercer/visuel1.jpg)

Pour inviter le travailleur à adopter une posture dynamique en se
musclant le corps. 

Au contraire des propositions existantes fabriquées avec des matériaux
issus de la pétrochimie, notre gamme est totalement écoresponsable.

Nous assemblons des éléments recyclés et des éléments écoconçus.Nous
utilisons leurs diverses performances d'élasticité, pour créer un objet
intuitif et dynamique associé à un mode d'emploi et à des exemples
pratiques.

|||
|-|-|
|![Sport et pieds](/images/exposition/s_exercer/visuel2.jpg)|![Sport et jambe](/images/exposition/s_exercer/visuel3.jpg)|

Ce projet, conçu et structuré autour d'une réflexion commune, propose
des réponses et des regards multiples, ouverts sur des possibles plus
responsables. Il s'appuie sur une cartographie de besoins et de
fonctionnalités textiles en intégrant un cahier des charges low-tech
défini par quelques grands principes : frugalité et économie à la
source, conception basée sur des techniques durables et réparables,
conditions de production s'appuyant sur le savoir et un travail digne.
Il privilégie la sobriété d'usage + la qualité d'un bien ou d'un
service.

Il se lit comme un ensemble de recherches, un cahier d'idées, de
matières, de scénarios, d'inspirations : des rêves avec leur mode
d'emploi, des utopies concrètes. Il a été mené dans le cadre d'un
Atelier de Projet d'un se- mestre à l'ENSCI-Les Ateliers, dans le
département textile, d'octobre 2020 à janvier 2021, par : *Alice
Bertrand, Nina Capron, Ines de Chefdebien Zagarriga, Floriane Delaville,
Camille Ferrer Gueldry, Camille Foucher, Manon Jacob, Lucie Le Jeune,
Cécile Lenoble, Anaïs Le Gou- pil, Victoire Lesthevenon, Laurie
Sapina* ;

Encadrées par *Hélène Lemaire, Marion Lévy,* Et la collaboration
de *Vanessa Goetz, Tony Jouanneau, Hélène Lemaire, Marion Lévy, Giovanna
Massoni*.

