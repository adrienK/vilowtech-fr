---
title: "Se chausser"
miniature: "/images/exposition/miniature/se_chausser.png"
description: "Se chausser"
exposition:
  nom: "Se chausser"
  projet: "Système de construction de chaussure en open source basé sur le réemploi de matières premières locales."
credit:
  nom: "Nina Capron & Lucie Le Jeune, Ensci, 2019"
  url: ""
notes:
    - "Video de presentation https://youtu.be/e-jEnIJWRk0"
---

Il s'adresse à une *communauté* intéressée par le « faire soi-même » :
de l'autodidacte au maker en Fab Lab. Il est déclinable selon les
différents gisements de matières disponibles et s'accompagne de
documents pédagogiques.

![Preparation des matières premières](/images/exposition/se_chausser/visuel1.jpg)

Peu investie par la communauté lowtech, la chaussure répond pourtant à
un besoin essentiel : protéger son pied du sol pour faciliter la marche.
Aujourd'hui, elle incarne l'archétype d'un objet mondialisé composé de
matières provenant de l'industrie pétrochimique.

Le point de départ est la semelle en tant qu'élément fondateur de la
chaussure et de la marche. Des principes de construction d'assemblages
simples corrélés à des matériaux issus de notre gisement génèrent des
spécimens de chaussures fonctionnelles. Dans une logique de réparabilité
et de récupération en évitant toute colle et liant, elles participent à
un cycle de vie vertueux.

|||
|-|-|
|![Vue semelle](/images/exposition/se_chausser/visuel2.jpg)|![Vue pied](/images/exposition/se_chausser/visuel3.jpg)|

Ce projet, conçu et structuré autour d'une réflexion commune, propose
des réponses et des regards multiples, ouverts sur des possibles plus
responsables. Il s'appuie sur une cartographie de besoins et de
fonctionnalités textiles en intégrant un cahier des charges low-tech
défini par quelques grands principes : frugalité et économie à la
source, conception basée sur des techniques durables et réparables,
conditions de production s'appuyant sur le savoir et un travail digne.
Il privilégie la sobriété d'usage + la qualité d'un bien ou d'un
service.

Il se lit comme un ensemble de recherches, un cahier d'idées, de
matières, de scénarios, d'inspirations : des rêves avec leur mode
d'emploi, des utopies concrètes. Il a été mené dans le cadre d'un
Atelier de Projet d'un se- mestre à l'ENSCI-Les Ateliers, dans le
département textile, d'octobre 2020 à janvier 2021, par : *Alice
Bertrand, Nina Capron, Ines de Chefdebien Zagarriga, Floriane Delaville,
Camille Ferrer Gueldry, Camille Foucher, Manon Jacob, Lucie Le Jeune,
Cécile Lenoble, Anaïs Le Gou- pil, Victoire Lesthevenon, Laurie
Sapina* ;

Encadrées par *Hélène Lemaire, Marion Lévy*, Et la collaboration
de *Vanessa Goetz, Tony Jouanneau, Hélène Lemaire, Marion Lévy, Giovanna
Massoni*.

