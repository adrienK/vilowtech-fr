---
title: Se laver
miniature: /images/exposition/miniature/se_laver.png
description: Réflexion sur les "standards de conforts" et proposition de design prospectif visant à redéfinir la salle de bain.
exposition:
  nom: L'eau s'écoule
  projet: système de douche avec chauffage à infrarouge
credit:
  nom: Joachim Savin, Ensci, Les Ateliers, 2019
  url: https://www.ensci.com/
notes:
    - Video de presentation https://www.youtube.com/watch?v=x7DUUZfER4g
---

Issus d’une réflexion sur les « standards de conforts » l’eau s’écoule est un projet de design prospectif visant à redéfinir la façon dont nous vivons au sein d’espaces domestiques.

![mise en situation](/images/exposition/se_laver/situation.jpg)

L’organisation actuelle de la salle de bain induit des comportements nécessairement consommateurs d’eau et d’électricité. Ce projet est une proposition architecturale prospective, une reconfiguration totale de l’espace et de la notion de confort contemporain, hérité des années 50.

![visuel du projet](/images/exposition/se_laver/visuel.jpg)

Nous sommes partis du constat que plus de 90% de l’eau consommée pendant une douche est utilisée pour se réchauffer, et non pour se laver, afin de redessiner cet espace.

En partenariat avec l’entreprise Verelec, nous avons dessiné un système de chauffage à infrarouge long qui répond au besoin de chaleur sans utiliser d’eau. Partant de cet objet nous avons redéfini de nouveaux rituels, gestes et objets permettant de n’utiliser que 8 litres d’eau potable par jour et par personne, contre 80 litres habituellement. Il est ainsi possible de diviser la consommation d’eau par 10 tout en réduisant la consommation d’électricité de 30% sans perdre en soin ni en plaisir.

![plan de realisation](/images/exposition/se_laver/plan.jpg)
