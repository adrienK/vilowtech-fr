---
title: "Se nourrir"
miniature: "/images/exposition/miniature/se_nourrir.png"
description: "Le Labo Glaneur"
exposition:
  nom: "Le Labo Glaneur"
  projet: "Vers une science humaine du recyclage"
credit:
  nom: "Orianne Collet, ESAA Boulle, 2020"
  url: ""
notes:
    - "Video de presentation https://youtu.be/CkvSwy2zn1A"
---

En 15OOO ans d'existence, les glaneurs se sont adaptés aux différents
environnements urbains afin de ramasser les restes alimentaires. Le
glanage nous rappelle la valeur humaine de « prendre soin ».
Aujourd'hui, leurs expériences quotidiennes témoignent d'un rapport
singulier à la nourriture et aux objets. Comment leur garantir un droit
à la marge dans notre monde normalisé ?

L'objectif est d'améliorer le système de ramassage post-marchés, pour
limiter le gaspillage des denrées et permettre à chacun d'être
responsable. Ma démarche est de rendre possible la synergie entre les
acteurs du marché et les élus municipaux. Les méthodes et outils
participatifs que je propose peuvent déclencher ces synergies.

En tant que designer, j'ai imaginé le  » Labo Glaneur », une initiative
organisée en association qui viserait à revaloriser le périssable. Mon
action s'est déroulée en quatre étapes :

1. Étude de terrain auprès des publics concernés : ramasseurs, commerçants, glaneurs.
2. Recherche de scénarios illustrés sur les droits de glanage possibles.
3. Prototypage de cinq objets de revalorisation : la cloche, le four solaire, le frigo urbain, le germoir, le séchoir solaire.
4. Création pour les glaneurs, de guides d'auto-construction illustrés pour la réalisation frugale de ces objets.

![Les brochuress](/images/exposition/se_nourrir/visuel1.jpg)

