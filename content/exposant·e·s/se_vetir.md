---
title: "Se vêtir"
miniature: "/images/exposition/miniature/se_vetir.png"
description: "En trois temps"
exposition:
  nom: "En trois temps"
  projet: "Système vestimentaire de trois couches qui s'additionnent"
credit:
  nom: "Manon Jacob & Cécile Lenoble, Ensci, 2021"
  url: ""
notes:
    - "Video de presentation https://youtu.be/IGWjjNuu__Q"
---
Un système vestimentaire de trois couches qui s'additionnent : une
première peau thermorégulatrice, une seconde peau-parure colorée et
sensible, une troisième communicante et militante.

![Pieces de tissue](/images/exposition/se_vetir/visuel1.jpg)

Le système en trois peaux offre une modularité qui couvre les
fonctionnalités du vêtement ; la première couche protège ; la seconde
personnalise ; la troisième est support d'expression.

De multiples combinaisons sont possibles, adaptées à chacun·e et
permettent de réduire la consommation

Le système se compose de pièces modulables, réparables, biodégradables
et recyclables, en privilégiant un circuit local.

|||
|-|-|
|![Echarpe face](/images/exposition/se_vetir/visuel2.jpg)|![Echarpe dos](/images/exposition/se_vetir/visuel3.jpg)|

Ce projet, conçu et structuré autour d'une réflexion commune, propose
des réponses et des regards multiples, ouverts sur des possibles plus
responsables. Il s'appuie sur une cartographie de besoins et de
fonctionnalités textiles en intégrant un cahier des charges low-tech
défini par quelques grands principes : frugalité et économie à la
source, conception basée sur des techniques durables et réparables,
conditions de production s'appuyant sur le savoir et un travail digne.
Il privilégie la sobriété d'usage + la qualité d'un bien ou d'un
service.

Il se lit comme un ensemble de recherches, un cahier d'idées, de
matières, de scénarios, d'inspirations : des rêves avec leur mode
d'emploi, des utopies concrètes. Il a été mené dans le cadre d'un
Atelier de Projet d'un se- mestre à l'ENSCI-Les Ateliers, dans le
département textile, d'octobre 2020 à janvier 2021, par : *Alice
Bertrand, Nina Capron, Ines de Chefdebien Zagarriga, Floriane Delaville,
Camille Ferrer Gueldry, Camille Foucher, Manon Jacob, Lucie Le Jeune,
Cécile Lenoble, Anaïs Le Gou- pil, Victoire Lesthevenon, Laurie
Sapina* ;

Encadrées par *Hélène Lemaire, Marion Lévy, *Et la collaboration
de *Vanessa Goetz, Tony Jouanneau, Hélène Lemaire, Marion Lévy, Giovanna
Massoni*.

