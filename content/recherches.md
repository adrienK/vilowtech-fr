---
title: Recherches
menu: main
weight: 10
colonnes: true
module: 
    load: recherche
---

Depuis quelques mois Villette Makerz collabore avec AZIMIO pour explorer le sujet des « innovations low-tech », avec le soutien de l’Agence de la transition énergétique (ADEME). Alors que les technologies ont acquis une place prépondérante dans nos façons de produire, de consommer et de vivre, la démarche low-tech (communément définie par opposition aux high-tech) interroge le « juste » niveau de technologies à employer en fonction des usages.

Au cœur du Parc de la Villette, nous souhaitons explorer les intérêts et les enjeux pour des institutions culturelles à se saisir de mouvements culturels actuels tels que la transition écologique, ou encore les low-techs.

Espace d’accueil du public et d’expression des artistes qui y sont invité.e.s, une institution culturelle est un lieu de représentation fort, invitant à vivre des expériences sensibles individuelles et collectives uniques. Un espace où penser et expérimenter des pratiques émergentes par la création d’œuvres, la production d’événements et le mode d’accueil des publics. Un lieu de diffusion d’images et d’artefacts, venant interroger notre regard sur les choses et renouveler notre rapport au monde.

Notre question est donc la suivante : 
