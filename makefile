# this makefile is used to run the server test, showing the helper or running the builder

# the default target is the helper message
default:
	@echo -e "Vilowtech.fr makefile helper:"
	@echo "	make css    - Build hugo and cp the css"
	@echo "	make serve  - Run the dev server"
	@echo "	make build  - Run the builder"
	@echo "	make helper - Show this message"

# run the server
serve:
	@hugo serve --ignoreCache --noHTTPCache --disableFastRender --debug

# run the builder
build:
	@./tools/builder.sh

build_css:
	@hugo
	@cp public/css/main.css themes/livreblanc/static/css/main.css

# build the html statistics	page
stats:
	@zcat -f ~/logs/*vilowtechfr* | ./tools/goaccess ~/logs/*vilowtechfr*.log - --ignore-crawlers -a --anonymize-ip --anonymize-level=2 --log-format=combined --geoip-database=./tools/dbip-country-lite.mmdb > report.html
