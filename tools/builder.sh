#!/bin/bash
script="$(dirname "$(readlink -f "$0")")"
root="$(dirname "$script")"
log="$root/log.txt"

data_dir="$root/data"
static_dir="$root/static"
public_dir="$root/public"
content_dir="$root/content"

TRUE=0
FALSE=1

has_changed() {
    if [ $# -lt 1 ]; then
        echo "this function needs at least one argument" >&2
        return $FALSE
    fi

    for folder in "$@"; do
        if [[ $(find "$folder" -type f -mtime -0.75 | wc -l) -gt 0 ]]; then
            return $TRUE
        fi
    done

    return $FALSE
}

# Signal in the log file that the script is running
echo -e "\n[$(date)] Starting the builder" >> "$log"

# Check if we need to run the image optimizer
if has_changed "$static_dir"; then
    echo -e "\n[OPTIM] Static files have changed, optimizing images" >> "$log"
    "$script/img_size.sh" 2>&1 | tee -a "$log"
fi

# Check if we need to rebuild the site or if this is the first run
if [[ -z $(ls -A "$public_dir") ]] || has_changed "$content_dir" "$data_dir"; then
    echo -e "\n[HUGO] Content or data has changed, rebuilding site" >> "$log"
    "hugo" -s "$root" 2>&1 | tee -a "$log"
fi