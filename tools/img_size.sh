#!/bin/bash
script="$(dirname "$(readlink -f "$0")")"
root="$(dirname "$script")"

images="$(find "$root/static" -type f -regextype posix-extended -regex '.*\.(jpe?g|png|webp)')"

for image in $images; do
    width=$(identify -format "%w" "$image")
    if [[ $width -gt 1500 ]]; then
        echo "Resizing $image from $width to 1500px"
        convert "$image" -resize 1500x "$image"
    fi

    size=$(stat -c%s "$image")
    if [[ $size -lt 120000 ]]; then
        # echo "No need to compress $image size: $size"
        continue
    fi

    case "$image" in
        *.jpg|*.jpeg)
            mogrify -sampling-factor 4:2:0 -strip -quality 50 -interlace JPEG -colorspace sRGB "$image"
            ;;
        *.png)
            mogrify --strip "$image"
            ;;
        *.webp)
            mogrify -define webp:lossless=false -define method=6 -define preprocessing=2 -define thread-level=1 -define partitions=3 "$image"
            ;;
    esac

    echo "Compressed $image from $size done"
done
